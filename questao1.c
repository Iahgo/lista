#include <stdio.h>

int main (){
	
	float tam;
	
	printf("Qual o valor em metros?\n"); 
	scanf(" %f", &tam);
	printf("Tamanho em decimetro: %f dm\n",tam*10);
	printf("Tamanho em centimetro: %f cm\n",tam*100);
	printf("Tamanho em milimetro: %f mm\n",tam*1000);


	return 0 ;

}

